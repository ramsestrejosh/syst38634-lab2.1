package time;

import static org.junit.Assert.*;

import org.junit.Test;

/** 
 * 
 * @author ramses trejo
 *
 */

public class TimeTest {
	
	

	@Test
	public void testGetTotalSecondsRegular( ) {
		int totalSeconds = Time.getTotalSeconds( "01:01:01" );
		assertTrue( "The time provided does not match the result" , totalSeconds == 3661 );
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException( ) {
		int totalSeconds = Time.getTotalSeconds( "01:01:0A" );
		fail( "The time provided is not valid" );
	} 	
	
	@Test 
	public void testGetTotalSecondsBoundaryIn( ) {
		int totalSeconds = Time.getTotalSeconds( "00:00:59" );
		assertTrue( "The time provided does not match the result" , totalSeconds == 59 );		
	} 	

	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut( ) {
		int totalSeconds = Time.getTotalSeconds( "01:01:60" );
		fail( "The time provided is not valid" );
	} 		
	
}
